import os

import aiofiles.os
import magic
import logging

from nio import AsyncClient, UploadResponse, ErrorResponse, RoomGetStateEventError

from sticker_types import MatrixStickerset


async def send_text_to_room(client: AsyncClient, room_id: str, message: str):
    content = {
        "msgtype": "m.notice",
        "body": message,
    }
    return await client.room_send(
        room_id,
        "m.room.message",
        content,
    )

async def is_stickerpack_existing(client: AsyncClient, room_id: str, pack_name: str):
    response = (await client.room_get_state_event(room_id, 'im.ponies.room_emotes', pack_name))
    if isinstance(response, RoomGetStateEventError) and response.status_code == 'M_NOT_FOUND':
        return False
    return not response.content == {}


async def upload_stickerpack(client: AsyncClient, room_id: str, stickerset: MatrixStickerset):
    return await client.room_put_state(room_id, 'im.ponies.room_emotes', stickerset.json(), state_key=stickerset.name())


async def upload_image(client: AsyncClient, image: str):
    mime_type = magic.from_file(image, mime=True)
    file_stat = await aiofiles.os.stat(image)
    async with aiofiles.open(image, "r+b") as f:
        resp, maybe_keys = await client.upload(
            f,
            content_type=mime_type,
            filename=os.path.basename(image),
            filesize=file_stat.st_size,
        )
    if isinstance(resp, UploadResponse):
        logging.debug(f"Image {image} was uploaded successfully to server.")
        return resp.content_uri
    else:
        logging.error(f"Failed to upload image ({image}). Failure response: {resp}")
        return ""


async def upload_avatar(client: AsyncClient, image: str):
    avatar_mxc = await upload_image(client, image)
    if avatar_mxc:
        await client.set_avatar(avatar_mxc)
