import logging
import tempfile

from nio import MatrixRoom, AsyncClient
from nio.client.base_client import logged_in

from chat_functions import is_stickerpack_existing, send_text_to_room, upload_image, upload_stickerpack
from sticker_types import Sticker, MatrixStickerset
from telegram_exporter import TelegramExporter
from PIL import Image
from io import BytesIO

class MatrixReuploader:

    STATUS_OK = 0
    STATUS_NO_PERMISSION = 1
    STATUS_PACK_EXISTS = 2
    STATUS_PACK_EMPTY = 3

    STATUS_DOWNLOADING = 4
    STATUS_UPLOADING = 5
    STATUS_UPDATING_ROOM_STATE = 6

    def __init__(self, client: AsyncClient, room: MatrixRoom, exporter: TelegramExporter = None,
                 pack: list[Sticker] = None):

        if not exporter and not pack:
            raise ValueError('Either exporter or the pack must be set')

        self.client = client
        self.room = room
        self.exporter = exporter
        self.pack = pack

    async def _has_permission_to_upload(self) -> bool:
        return await self.client.has_event_permission(self.room.room_id, 'im.ponies.room_emotes', 'state')

    async def import_stickerset_to_room(self, pack_name: str, import_name: str):
        if not await self._has_permission_to_upload():
            yield self.STATUS_NO_PERMISSION
            return

        stickerset = MatrixStickerset(import_name)
        if await is_stickerpack_existing(self.client, self.room.room_id, stickerset.name()):
            yield self.STATUS_PACK_EXISTS
            return

        yield self.STATUS_DOWNLOADING
        converted_stickerset = await self.exporter.get_stickerset(pack_name, import_name)
        yield self.STATUS_UPLOADING
        for sticker in converted_stickerset:
            with tempfile.NamedTemporaryFile('w+b') as file:
                logging.info(len(sticker.image_data))
                if len(sticker.image_data) < 5000:
                    sticker.image_data += bytes(5000 - len(sticker.image_data))
                file.write(sticker.image_data)
                sticker_mxc = await upload_image(self.client, file.name)
            if sticker_mxc:
                image: Image.Image = Image.open(BytesIO(sticker.image_data)).convert("RGBA")
                (w, h) = image.size
                if w > 256 or h > 256:
                    if w > h:
                        h = int(h / (w / 256))
                        w = 256
                    else:
                        w = int(w / (h / 256))
                        h = 256
                stickerset.add_sticker(sticker_mxc, sticker.alt_text, h, w, len(sticker.image_data))

        if not stickerset.count():
            yield self.STATUS_PACK_EMPTY
            return

        yield self.STATUS_UPDATING_ROOM_STATE
        await upload_stickerpack(self.client, self.room.room_id, stickerset)

        yield self.STATUS_OK
