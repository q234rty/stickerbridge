from multiprocessing import Pool
import os
import logging
from typing import List

from lottie.importers import importers
from lottie.exporters import exporters
from telethon import TelegramClient
from telethon.errors import StickersetInvalidError
from telethon.tl.functions.messages import GetStickerSetRequest
from telethon.tl.types import InputStickerSetShortName

from io import BytesIO
from PIL import Image

from sticker_types import Sticker
import tempfile
from subprocess import DEVNULL, STDOUT, check_call

def _convert_image(data: bytes):
    image: Image.Image = Image.open(BytesIO(data)).convert("RGBA")
    new_file = BytesIO()
    image.save(new_file, "webp")
    return new_file.getvalue()


def _convert_animation(data: bytes, width=0, height=0):
    importer = None
    for p in importers:
        logging.info(p.slug)
        if "tgs" in p.extensions:
            importer = p
            break
    exporter = exporters.get_from_filename('233.webp')
    an = importer.process(BytesIO(data))

    an.frame_rate = 24

    if width or height:
        if not width:
            width = an.width * height / an.height
        if not height:
            height = an.height * width / an.width
        an.scale(width, height)

    out = BytesIO()
    exporter.process(an, out)
    return out.getvalue()

def _convert_webm(data: bytes):
    fdin, pathin = tempfile.mkstemp(suffix=".webm")
    fdout, pathout = tempfile.mkstemp(suffix=".webp")
    os.write(fdin, data)
    #ret = check_call(['/usr/bin/ffmpeg','-y','-c:v','libvpx-vp9','-i',f"'{pathin}'",'-loop','0',f"'{pathout}'"], stdout=DEVNULL, stderr=STDOUT)
    os.system("ffmpeg -y -c:v libvpx-vp9 -i '"+pathin+"' -loop 0 -quality 60 -compression_level 6 '"+pathout+"' > /dev/null")
    #raise ValueError
    ans = os.read(fdout,233333333)
    os.close(fdin)
    os.close(fdout)
    os.remove(pathin)
    os.remove(pathout)
    return ans
        
def _process_sticker(document) -> Sticker:
    alt = document[1]
    if document[0].mime_type == 'image/webp':
        data = _convert_image(document[0].downloaded_data_)
    if document[0].mime_type == 'application/x-tgsticker':
        data = _convert_animation(document[0].downloaded_data_)
    if document[0].mime_type == 'video/webm':
        data = _convert_webm(document[0].downloaded_data_)
    return Sticker(data, alt)


class TelegramExporter:
    def __init__(self, api_id: int, api_hash: str, bot_token: str, secrets_filename: str):
        self.api_id = api_id
        self.api_hash = api_hash
        self.bot_token = bot_token
        self.secrets_filename = secrets_filename

        self.client = TelegramClient(self.secrets_filename, self.api_id, self.api_hash)

    async def connect(self):
        await self.client.start(bot_token=self.bot_token)

    async def get_stickerset(self, pack_name: str, import_name: str) -> list[Sticker]:
        result: List[Sticker] = list()

        try:
            sticker_set = await self.client(GetStickerSetRequest(InputStickerSetShortName(short_name=pack_name), hash=0))
        except StickersetInvalidError:
            return result  # return empty on fail

        downloaded_documents = []
        for document_data in sticker_set.documents:
            document_data.downloaded_data_ = await self.client.download_media(document_data, file=bytes)
            downloaded_documents.append(document_data)
            
        for i in range(len(downloaded_documents)):
            downloaded_documents[i] = (downloaded_documents[i], import_name+"-"+str(i))
        
        pool = Pool() 
        result = pool.map(_process_sticker, downloaded_documents)

        return result
